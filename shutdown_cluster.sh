#!/bin/bash


echo "Determine which node is actually master..."

MASTER_DRBD=$(sudo crm_mon -1|grep 'Masters')
SLAVE_DRBD=$(sudo crm_mon -1|grep 'Slaves')

MASTER_CLUSTER_IFX_1=$(curl -s -G 'http://172.16.1.1:8086/query' --data-urlencode "db=app" --data-urlencode "q=SELECT last(master) from cluster")
MASTER_CLUSTER_IFX_2=$(curl -s -G 'http://172.16.1.2:8086/query' --data-urlencode "db=app" --data-urlencode "q=SELECT last(master) from cluster")

shutdown_cluster()
{
	MASTER_FINAL=$1
	echo "${MASTER_FINAL}"
}

if [ "${MASTER_CLUSTER_IFX_1}" == "${MASTER_CLUSTER_IFX_2}" ]; then
	echo "Same Master (ifx), looks good!"
	SAME_IFX="True"
else
	echo "Masters (ifx) seems to be differents !!"
	SAME_IFX="False"
fi

if [ "${SAME_IFX}" == "True" ] && [ "${MASTER_CLUSTER_IFX_1}" == "${MASTER_DRBD}" ]; then
	echo "Same Master ifx/drbd, looks good ++ !"
  if [ "${MASTER_DR}" == "node-master" ]; then
  	MASTER_FINAL="node-master"
  else
  	MASTER_FINAL="node-slave"
  fi
  shutdown_cluster "${MASTER_FINAL}"
else
	echo "Master (ifx/drbd) seems to be differents !!"
fi