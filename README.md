# ha-corosync-pacemaker-dashboard

## Getting Started

The goal is to produce interactive dashboard to control cluster.

## Requirements

- python
- Flask
- json
- subprocess

## Step

1 - Execute `cluster` from one node of the cluster to get cluster monitoring datas

2 - Parse the json

3 - Display informations 

## Example
`
{
    "Cluster": {
        "LoadAverage": {
            "status": {
                "node-master": 0.11,
                "node-slave": 0
            }
        },
        "Resources": {
            "status": {
                "DrbFS": "Started node-master",
                "Supervisor": "Started node-master",
                "vip": "Started node-master"
            }
        },
        "Roles": {
            "status": {
                "master": "node-master",
                "slave": "node-slave"
            }
        },
        "Sensors": {
            "status": {
                "node-master": "11°",
                "node-slave": "18°"
            }
        },
        "StatusNode": {
            "status": {
                "node-master": "online",
                "node-slave": "online"            }
        },
        "Uptime": {
            "status": {
                "node-master": " 4:48",
                "node-slave": " 4:48"
            }
        }
    }
}
`

## Author

* **Rémy Zygas** - *Initial work* - [r.zygas@gmail.com](mailto:r.zygas@gmail.com)

