# -*- coding: utf-8 -*-

from subprocess import check_output as run
import os
import json

if os.getenv('FLASK_ENV') == 'develop':
    JSON_PATH = 'status.json' # UpNrun master/slave Up2date
    # JSON_PATH = 'status_no_slave.json' # UpNrun master only WFConnection
    # JSON_PATH = 'status_shutdown.json' # Nodes are down
else:
    JSON_PATH = '/cluster/cluster-latest.json'


def get_status():
    """ Load corosync/pacemaker status writted into json """
    with open(JSON_PATH) as f:
        data = json.load(f)
    return data['Cluster']


def is_app_run():
    """ Check if we can HTTP/GET http://mep.url/server_details.json"""
    if os.getenv('FLASK_ENV') == 'develop':
        return True 
    if is_up() is True:
        get_server_details = run('curl -o /dev/null -s -w %{http_code} -m 20 \"http://172.16.0.1/server_details.json\" ; exit 0', shell=True)
        if get_server_details == '200':
            return True
        else:
            return False
    else:
        return False


def is_master():
    """ Check who is master and return his ip address """
    is_master_status = {}
    if len(get_status()['Roles']['status']) > 0:
        master = get_status()['Roles']['status']['master']
        if master == 'node-master':
            master_ip = '172.16.1.1'
        else:
            master_ip = '172.16.1.2'
        is_master_status['master'] = master
        is_master_status['master_ip'] = master_ip
    else:
        is_master_status['master'] = ''
        is_master_status['master_ip'] = ''    
    return is_master_status


def is_slave():
    """ Check who is slave and return his ip address """
    is_slave_status = {}
    if len(get_status()['Roles']['status']) > 1:
        slave = get_status()['Roles']['status']['slave']
        if slave == 'node-master':
            slave_ip = '172.16.1.1'
        else:
            slave_ip = '172.16.1.2'
        is_slave_status['slave'] = slave
        is_slave_status['slave_ip'] = slave_ip
    else:
        is_slave_status['slave'] = ''
        is_slave_status['slave_ip'] = ''
    return is_slave_status


def is_up():
    """ Check is vip address is up  """  
    if os.getenv('FLASK_ENV') == 'develop':
        is_up_status = True
    else:
        if 'is up' in run('nmap -sP --max-retries=1 --host-timeout=750ms 172.16.0.1', shell=True):
            is_up_status = True
        else:
            is_up_status = False
    return is_up_status


def power_on():
    """ Send magic-packet (WakeOnLan) to the cluster's nodes """
    if os.getenv('FLASK_ENV') == 'develop':
        run('touch /tmp/etherwake_node1', shell=True)
        run('touch /tmp/etherwake_node2', shell=True)
    else:
        run('etherwake 00:1f:c6:9c:c1:7e', shell=True)
        run('etherwake 00:1f:c6:9c:b2:d4', shell=True)


def power_off():
    """ Call shutdown script on each node, start with slave then master """
    master_ip = is_master()['master_ip']
    slave_ip = is_slave()['slave_ip']
    if os.getenv('FLASK_ENV') == "develop":
        run('echo "ssh -o ConnectTimeout=2 -o BatchMode=yes login@%s \'sudo poweroff\'" > /tmp/shutdown_slave' % slave_ip, shell=True)
        run('echo "ssh -o ConnectTimeout=2 -o BatchMode=yes login@%s \'sudo poweroff\'"> /tmp/shutdown_master' % master_ip, shell=True)
    else:
        # run('ssh -o ConnectTimeout=2 -o BatchMode=yes login@%s \'sudo /home/login/shutdown\'' % slave_ip, shell=True)
        # run('ssh -o ConnectTimeout=2 -o BatchMode=yes login@%s \'sudo /home/login/shutdown\'' % master_ip, shell=True)
        run('ssh -o ConnectTimeout=2 -o BatchMode=yes login@%s \'sudo poweroff\'; exit 0' % slave_ip, shell=True)
        run('ssh -o ConnectTimeout=2 -o BatchMode=yes login@%s \'sleep 1 && sudo poweroff\'; exit 0' % master_ip, shell=True)


def power_off_without_slave():
    """ Call shutdown script on master only """
    master_ip = is_master()['master_ip']
    if os.getenv('FLASK_ENV') == 'develop':
        run('echo "ssh -o ConnectTimeout=2 -o BatchMode=yes login@%s \'sudo poweroff\'"> /tmp/shutdown_master' % master_ip, shell=True)
    else:
        run('ssh -o ConnectTimeout=2 -o BatchMode=yes login@%s \'sudo /home/login/shutdown\'; exit 0' % master_ip, shell=True)


def check_if_slave():
    """ Check if there is a slave in the cluster.
    Return True if slave present. Return False if no slave present """
    Roles = get_status()['Roles']['status']
    master = 'debug_value'
    slave = 'debug_value'
    for role, node in Roles.items():
        if role == 'master':
            master = node
        elif role == 'slave':
            slave = node
    if slave == 'node-master':
        check_if_slave_status = True
    elif slave == 'node-slave':
        check_if_slave_status = True
    else:
        check_if_slave_status = False
    return check_if_slave_status


def health():
    """
    OK: if master True and slave True
          if node1_overview 'UpToDate' and node2_overview 'UpToDate'
            if /var/www ext4
              if Resources started
    WARNING:  if  master True                
                if node1_overview UpToDate/DUnknown and or node2_overview UpToDate/DUnknown
                  if /var/www/ ext4
                    if Resources started
    KO: if node1_overview /var/www/ ext4 and node2_overview /var/www/ ext4
        else Resources stoped
    """
    cluster_health_status = 'debug_value'
    node1_overview = 'debug_value'
    node2_overview = 'debug_value'
    master_is_up = 'debug_value'
    slave_is_up = 'debug_value'
    node1_status = 'debug_value'
    node2_status = 'debug_value'
    if check_if_slave() is True:
        if os.getenv('FLASK_ENV') == 'develop':
            # OK 
            node1_overview = ' 0:DrbdMedia/0  Connected Primary/Secondary UpToDate/UpToDate /var/www ext4 123G 1.2G 116G 2%'
            node2_overview = ' 0:DrbdMedia/0  Connected Secondary/Primary UpToDate/UpToDate'
            # WARNING
            # node1_overview = ' 0:DrbdMedia/0  WFConnection Primary/Unknown UpToDate/DUnknown /var/www ext4 123G 1.2G 116G 2%'
            # node2_overview = ' 0:DrbdMedia/0  Unconfigured . .'
            # KO             
            # node1_overview = ' 0:DrbdMedia/0  WFConnection Primary/Unknown UpToDate/DUnknown /var/www ext4 123G 1.2G 116G 2%'
            # node2_overview = ' 0:DrbdMedia/0  WFConnection Primary/Unknown UpToDate/DUnknown /var/www ext4 123G 1.2G 116G 2%'
        else:
            node1_overview = run('ssh -o ConnectTimeout=2 -o BatchMode=yes login@172.16.1.1 \'sudo drbd-overview\'; exit 0', shell=True)
            node2_overview = run('ssh -o ConnectTimeout=2 -o BatchMode=yes login@172.16.1.2 \'sudo drbd-overview\'; exit 0', shell=True)
    elif is_up() is True:
        if os.getenv('FLASK_ENV') == 'develop':
            node1_overview = ' 0:DrbdMedia/0  WFConnection Primary/Unknown UpToDate/DUnknown /var/www ext4 123G 1.2G 116G 2%'
        else:
            node1_overview = run('ssh -o ConnectTimeout=2 -o BatchMode=yes login@172.16.0.1 \'sudo drbd-overview\'; exit 0', shell=True)
    else:
        cluster_health_status = 'KO'

    if is_master()['master']:
        master_is_up = True
    if is_slave()['slave']:
        slave_is_up = True

    if 'UpToDate/UpToDate' in node1_overview:
        node1_status = 'UpToDate'
    if 'UpToDate/UpToDate' in node2_overview:
        node2_status = 'UpToDate'
    if 'UpToDate/DUnknown' in node1_overview:
        node1_status = 'WFConnection'
    if 'UpToDate/DUnknown' in node2_overview:
        node2_status = 'WFConnection'

    if master_is_up and slave_is_up is True:
        if 'UpToDate' in node1_status and 'UpToDate' in node2_status:
            if 'ext4' in node1_overview or node2_overview:
                if not 'Stopped' in get_status()['Resources']['status'].values():
                    cluster_health_status = 'OK'

    if master_is_up is True:
        if 'WFConnection' in node1_status:
            # print 'WAR/wfc1'
            if 'ext4' in node1_overview:
                cluster_health_status = 'WARNING'
                # print 'WAR/ext4-1 found'
            if not 'Stopped' in get_status()['Resources']['status'].values():
                cluster_health_status = 'WARNING'
                # print 'WAR/1not-stop'
        elif 'WFConnection' in node2_status:
            # print 'WAR/wfc2'
            if 'ext4' in node2_overview:
                cluster_health_status = 'WARNING'
                # print 'WAR/ext4-2 found'
            if not 'Stopped' in get_status()['Resources']['status'].values():
                cluster_health_status = 'WARNING'
                # print 'WAR/2not-stop'

    if 'ext4' in node1_overview and 'ext4' in node2_overview:
        cluster_health_status = 'KO'
    if 'Stopped' in get_status()['Resources']['status'].values():
        cluster_health_status = 'KO'

    return cluster_health_status
