# -*- coding: utf-8 -*-

from flask import Flask, redirect, url_for, render_template
import models as cluster

app = Flask(__name__)

@app.route('/', methods=['GET'])
def home():
    return render_template('cluster.html')


@app.route('/cluster_status', methods=['GET'])
def cluster_status():
    cluster_health_status = cluster.health()
    is_mep_run_status = cluster.is_mep_run()
    nodes_up = 0
    if cluster.is_up() is True:
        nodes_up += 1
    if nodes_up > 0:
        return render_template('cluster_status.html', cluster_health_status=cluster_health_status, is_mep_run_status=is_mep_run_status)
    return render_template('cluster_start_form.html')


@app.route('/cluster_start', methods=['GET'])
def cluster_start():
    cluster.power_on()
    # return to delay loader (25s) then return redirect(url_for('home'))
    return render_template('cluster_start_waiting.html')


@app.route('/cluster_stop', methods=['GET'])
def cluster_stop():
    return render_template('stop_cluster_confirmation.html')


@app.route('/cluster_stop_confirmation', methods=['GET'])
def cluster_stop_confirmation():
    if cluster.check_if_slave() is True:
        cluster.power_off()
    else:
        cluster.power_off_without_slave()
    # return to delay loader (15s) then return redirect(url_for('home'))
    return redirect(url_for('home'))



@app.route('/cluster_details', methods=['GET'])
def cluster_details():
    details = cluster.get_status()
    Nodes = sorted(details['StatusNode']['status'].keys())
    StatusNode = details['StatusNode']['status']
    Roles = details['Roles']['status']
    Resources = details['Resources']['status']
    LoadAverage = details['LoadAverage']['status']
    Uptime = details['Uptime']['status']
    Sensors = details['Sensors']['status']
    return render_template('cluster_details.html', Nodes=Nodes, StatusNode=StatusNode, Roles=Roles, Resources=Resources, LoadAverage=LoadAverage, Uptime=Uptime, Sensors=Sensors)

if __name__ == '__main__':
    app.run(debug=False)
